# frozen_string_literal: true

class Redshift < RedshiftBase

  def self.execute_select_query(project, sql)
    readonly_connection(project)
    con = connection
    con.execute('set enable_result_cache_for_session=off;')
    con.select_all(sql).to_hash
  end

  def self.execute_write_query(project, sql)
    sys_connection(project)
    con = connection
    result = con.execute(sql)
    result.cmd_tuples
  end

  def self.create_shopify_customers(project)
    sql = "CREATE TABLE IF NOT EXISTS shopify_customers(
            id bigint NOT NULL,
            shopify_domain varchar(255) NOT NULL,
            email varchar(255) NOT NULL,
            accepts_marketing boolean,
            first_name varchar(255),
            last_name varchar(255),
            order_count integer,
            state varchar(255),
            total_spent varchar(255),
            last_order_id bigint,
            note varchar(255),
            verified_email boolean,
            multipass_identifier varchar(255),
            tax_exempt boolean,
            phone varchar(255),
            tags varchar(255),
            currency varchar(255),
            created_at varchar(255),
            updated_at varchar(255),
            PRIMARY KEY (id)
          );"
    execute_write_query(project, sql)
  end

  def self.create_shopify_orders(project)
    sql = "CREATE TABLE IF NOT EXISTS shopify_orders(
            id bigint NOT NULL,
            email VARCHAR(max) NOT NULL,
            total_price VARCHAR(max),
            total_tax VARCHAR(max),
            currency VARCHAR(max) ,
            financial_status VARCHAR(max) ,
            total_discounts VARCHAR(max) ,
            total_line_items_price VARCHAR(max) ,
            name VARCHAR(max) ,
            total_price_usd VARCHAR(max) ,
            processed_at VARCHAR(max) ,
            phone VARCHAR(max) ,
            customer_locale VARCHAR(max) ,
            browser_ip VARCHAR(max) ,
            order_number VARCHAR(max) ,
            processing_method VARCHAR(max) ,
            fulfillment_status VARCHAR(max) ,
            tags VARCHAR(max) ,
            contact_email VARCHAR(max) ,
            order_status_url VARCHAR(max) ,
            line_items VARCHAR(max) ,
            shipping_lines VARCHAR(max) ,
            billing_address VARCHAR(max) ,
            shipping_address VARCHAR(max) ,
            shopify_domain VARCHAR(max) NOT NULL,
            created_at VARCHAR(max),
            updated_at VARCHAR(max),
            PRIMARY KEY (id)
          );"
    execute_write_query(project, sql)
  end
end
