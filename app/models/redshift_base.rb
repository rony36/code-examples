# frozen_string_literal: true

class RedshiftBase < ApplicationRecord
  self.abstract_class = true

  def self.readonly_connection(project)
    redshift_info = {}
    redshift_info[:adapter] = :redshift
    redshift_info[:pool] = 5
    redshift_info[:port] = 5439
    redshift_info[:host] = project[:redshift_host]
    redshift_info[:database] = project[:redshift_database]
    redshift_info[:username] = project[:redshift_username]
    redshift_info[:password] = project[:redshift_password]

    establish_connection(redshift_info)
  end

  def self.sys_connection(project)
    redshift_info = {}
    redshift_info[:adapter] = :redshift
    redshift_info[:port] = 5439
    redshift_info[:pool] = 5
    redshift_info[:host] = project[:redshift_host]
    redshift_info[:database] = project[:redshift_database]
    redshift_info[:username] = project[:redshift_username]
    redshift_info[:password] = project[:redshift_password]

    establish_connection(redshift_info)
  end
end
