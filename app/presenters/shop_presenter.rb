# frozen_string_literal: true

class ShopPresenter < ApplicationPresenter
  presents :shop

  def render_vip?
    shop.advanced_plan?
  end

  def asset_name
    'widget/general'
  end

  def widget_type
    return 'standard' if h.params[:widget_type].blank?

    h.params[:widget_type]
  end

  def show_prompt?
    return false if shop.free_plan? || shop.basic_plan?
    return true if h.preview?
    return false unless CustomerNotification::PROMPT_NOTIFICATION_TYPES.include?(prompt_type.to_sym)
    return true if shop.customer_notifications.prompt.find_by(notification_type: prompt_type, active: true).present?

    false
  end

  def engagement_prompt_content
    prompt = shop.customer_notifications.prompt.find_by(notification_type: prompt_type)
    return {} if prompt.blank?

    {
      predefined_image: "prompt_graphics/#{prompt.notification_type}/#{prompt.safe_notification_setting(:predefined_banner)}.svg",
      custom_image_url: prompt.banner&.url,
      title: prompt_title(prompt),
      description: prompt_description(prompt),
      button_text: h.preview? && h.params[:button_text].present? ? h.params[:button_text] : prompt.safe_notification_setting(:btn_text)
    }
  end

  def prompt_description(prompt)
    content = h.preview? && h.params[:description].present? ? h.params[:description] : prompt.safe_notification_setting(:description)

    case prompt.notification_type
    when 'referral_sharing'
      content = content.gsub('{{reward_benefit}}', shop.referral_rewards.referring_customer.last.to_s)
    when 'points_spend'
      content = content.gsub('{{earned_points}}', "#{shop_customer.total_points} #{shop.points_currency}")
    else
      content
    end

    content
  end

  def launcher_btn_content
    launcher_icon = h.preview? && h.params[:launcher_icon].present? ? h.params[:launcher_icon] : shop.safe_widget_setting(:launcher_icon)
    launcher_text = h.preview? && h.params[:launcher_text].present? ? h.params[:launcher_text] : shop.safe_widget_setting(:launcher_text)

    h.content_tag :div, class: 'd-flex' do
      h.content_tag(:div, class: "btn-icon #{'d-none' if launcher_type == 'text'}") do
        if shop.launcher_custom_icon.present?
          h.image_tag(shop.launcher_custom_icon.url)
        else
          h.embedded_svg("launcher_icons/#{launcher_icon}.svg")
        end
      end +
        h.content_tag(:div, class: "btn-title align-items-center #{launcher_type == 'icon' ? 'd-none' : 'd-flex'}") do
          launcher_text
        end
    end
  end

  def widget_position
    return h.params[:position] if h.preview? && h.params[:position].present?

    shop.safe_widget_setting(:position)
  end

  def branding_logo
    shop.branding_logo&.url
  end

  def program_name
    return h.params[:program_name] if h.preview? && h.params[:program_name].present?

    shop.safe_widget_setting(:program_name).presence || shop.shop_name
  end

  def points_earn_ways
    shop.program_actions.map do |action|
      {
        id: action.id,
        title: action,
        subtitle: action.subtitle,
        icon: h.action_icon_path(action.program_action_type),
        branding_icon: action.branding_icon&.url
      }
    end
  end
end
