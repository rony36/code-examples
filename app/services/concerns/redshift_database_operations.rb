# frozen_string_literal: true

require 'active_support/concern'

module RedshiftDatabaseOperations
  extend ActiveSupport::Concern

  included do
    attr_reader :shop_domain, :remote_data

    def self.update(shop_domain, remote_data)
      new(shop_domain, remote_data).dump
    end

    def initialize(shop_domain, remote_data)
      @shop_domain = shop_domain
      @remote_data = remote_data
    end

    private

    def insert_data
      sql = create_sql(self.class::TABLE_NAME, sanitized_data)
      Redshift.execute_write_query(project_details, sql)

      { status: 200, message: 'insert success' }
    rescue StandardError => _e
      p _e
      { status: 400, message: 'Bad Request' }
    end

    def create_sql(table, rows)
      sql = "INSERT INTO #{table} (#{rows.keys.join(', ')}) VALUES ( "
      rows.values.inject do
        sql << '?,'
      end
      sql << '?  );'
      ActiveRecord::Base.send(:sanitize_sql_array, [sql].concat(rows.values))
    end

    def delete_row_if_exist
      sql = "DELETE FROM #{TABLE_NAME} WHERE id = #{remote_data['id']}"
      Redshift.execute_write_query(project_details, sql)

      { status: 200, message: 'sql query success' }
    rescue StandardError => _e
      { status: 400, message: 'Bad Request' }
    end

    def project_details
      {
        redshift_host: Rails.application.secrets.redshift_host,
        redshift_database: Rails.application.secrets.redshift_database,
        redshift_username: Rails.application.secrets.redshift_username,
        redshift_password: Rails.application.secrets.redshift_password,
      }
    end

    def sanitized_data
      attrs = self.class::REDSHIFT_FIELDS.map do |redshift_attr|
        remote_attr_data = remote_data[redshift_attr]
        [redshift_attr, (remote_attr_data.is_a?(Hash) || remote_attr_data.is_a?(Array) ? remote_attr_data.to_json.to_s : remote_attr_data)]
      end.to_h
      attrs['shopify_domain'] = shop_domain
      attrs
    end
  end
end
