# frozen_string_literal: true

class UpdateCustomerService
  include RedshiftDatabaseOperations

  TABLE_NAME = 'shopify_customers'
  REDSHIFT_FIELDS = %w[ id email accepts_marketing first_name last_name order_count last_order_id state total_spent
                       note verified_email multipass_identifier tax_exempt phone tags currency shopify_domain
                       created_at updated_at].freeze

  def dump
    shop = Shop.find_by(shopify_domain: shop_domain)
    return if shop.nil?

    # Create table if not exists
    Redshift.create_shopify_customers(project_details)
    delete_row_if_exist
    insert_data
  end
end
