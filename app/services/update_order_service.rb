# frozen_string_literal: true

class UpdateOrderService
  include RedshiftDatabaseOperations

  TABLE_NAME = 'shopify_orders'
  REDSHIFT_FIELDS = %w[id email total_price total_tax currency financial_status total_discounts total_line_items_price
                       name total_price_usd processed_at phone customer_locale browser_ip order_number processing_method
                       fulfillment_status tags contact_email order_status_url line_items shipping_lines billing_address
                       shipping_address shopify_domain created_at updated_at].freeze

  def dump
    shop = Shop.find_by(shopify_domain: shop_domain)
    return if shop.nil?

    Redshift.create_shopify_orders(project_details)

    # TODO: Have to make query something like this
    # query = <<~TEXT
    #   IF EXISTS (SELECT * FROM #{TABLE_NAME} WHERE ID = #{sanitized_data['id']})
    #   UPDATE #{TABLE_NAME} SET propertyOne = propOne, property2 . . .
    #   ELSE
    #   INSERT INTO #{TABLE_NAME} (propOne, propTwo . . .)
    # TEXT

    delete_row_if_exist
    insert_data
  end
end
