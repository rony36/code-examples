# frozen_string_literal: true

require 'openssl'
require 'base64'

class WebhookSignatureService
  attr_reader :data

  def self.signed_data(data)
    new(data).generate_signature
  end

  def initialize(data)
    @data = data
  end

  def generate_signature
    Base64.strict_encode64(OpenSSL::HMAC.digest(digest, key, data)).strip
  end

  def key
    Rails.application.secrets.webhook_secret
  end

  def digest
    OpenSSL::Digest.new('sha256')
  end
end
