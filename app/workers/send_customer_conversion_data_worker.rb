# frozen_string_literal: true

require 'rest-client'

class SendCustomerConversionDataWorker < ApplicationJob
  queue_as :low

  attr_reader :tracking_id

  class RetryNotAnError < RuntimeError; end

  def perform(tracking_id)
    @tracking_id = tracking_id

    response = RestClient.post(api_url, payload.to_json, request_headers)
    raise RetryNotAnError if response.code >= 400
  end

  def request_headers
    {
      content_type: :json,
      accept: :json,
      hmac: WebhookSignatureService.signed_data(payload.to_json)
    }
  end

  def payload
    @payload ||= { tracking_id: tracking_id }
  end

  def api_url
    @api_url ||= "http://#{Rails.application.secrets.booster_domain}/webhooks/product_purchased"
  end
end
